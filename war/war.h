#pragma once

#include <array>
#include <stdexcept>

enum Winner {
    FIRST,
    SECOND,
    NONE
};

struct GameResult {
    Winner winner;
    int turn;
};

GameResult SimulateWarGame(const std::array<int, 5>& first_deck, const std::array<int, 5>& second_deck) {
    throw std::runtime_error("Not implemented");
}

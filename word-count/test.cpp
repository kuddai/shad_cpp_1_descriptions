#include <testing.h>
#include <word_count.h>
#include <vector>

namespace tests {

void Small() {
    ASSERT_EQ(3, DifferentWordsCount("hello world e"));
    ASSERT_EQ(1, DifferentWordsCount("Test"));
    ASSERT_EQ(2, DifferentWordsCount("small,test,"));
}

void Zero() {
    ASSERT_EQ(0, DifferentWordsCount(""));
    ASSERT_EQ(0, DifferentWordsCount("    "));
    ASSERT_EQ(0, DifferentWordsCount(",123."));
    ASSERT_EQ(0, DifferentWordsCount("!"));
}

void Register() {
    ASSERT_EQ(4, DifferentWordsCount("hello Hello WORLD w,orld wOrld"));
    ASSERT_EQ(1, DifferentWordsCount("a A    a A"));
    ASSERT_EQ(3, DifferentWordsCount("    register REGISTER matters 'x'"));
}

void SomeTests() {
    ASSERT_EQ(2, DifferentWordsCount("12shit happens "));
    ASSERT_EQ(5, DifferentWordsCount(",,abc acb bac bca     cabCba    "));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Small);
    RUN_TEST(Zero);
    RUN_TEST(Register);
    RUN_TEST(SomeTests);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

cmake_minimum_required(VERSION 2.8)
project(dungeon)

if (TEST_SOLUTION)
  include_directories(../private/dungeon)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_dungeon test.cpp)

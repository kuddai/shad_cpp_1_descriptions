cmake_minimum_required(VERSION 2.8)
project(immutable-vector)

if (TEST_SOLUTION)
  include_directories(../private/scala-vector)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_immutable_vector
  test.cpp
  ../commons/catch_main.cpp)

#pragma once

#include <vector>
#include <stdexcept>

struct Student {
    std::string name, surname;
    int year, month, day;
};

enum class SortType {
    BY_NAME,
    BY_DATE
};

void SortStudents(std::vector<Student> *students, SortType sort_type) {
    throw std::runtime_error("Not implemented");
}

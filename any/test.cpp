#include <testing.h>
#include <any.h>
#include <string>

struct SomeStruct {
    int x;
};

namespace tests {

void TestSimple() {
    Any a(5);
    ASSERT_EQ(5, a.GetValue<int>());

    Any b(std::string("abacaba"));
    ASSERT_EQ("abacaba", b.GetValue<std::string>());

    Any c;
    c = 7.0;
    ASSERT_EQ(false, c.Empty());
    ASSERT_EQ(7.0, c.GetValue<double>());

    Any d;
    int *ptr = nullptr;
    d = ptr;
    ASSERT_EQ(false, d.Empty());
}

void EmptyTest() {
    Any a;
    ASSERT_EQ(true, a.Empty());

    std::vector<int> t{1, 2};
    Any b(t);
    ASSERT_EQ(false, b.Empty());
    a.Swap(b);

    ASSERT_EQ(t, a.GetValue<std::vector<int>>());
    ASSERT_EQ(false, a.Empty());
    ASSERT_EQ(true, b.Empty());

    a.Clear();
    ASSERT_EQ(true, a.Empty());
}

void CopyTest() {
    Any a(5);
    Any b = a;

    ASSERT_EQ(a.GetValue<int>(), b.GetValue<int>());

    Any c;
    c = b;

    ASSERT_EQ(b.GetValue<int>(), c.GetValue<int>());
    b.Clear();
    ASSERT_EQ(5, c.GetValue<int>());

    Any d(SomeStruct{3});
    ASSERT_EQ(3, d.GetValue<SomeStruct>().x);

    d = std::string("check");
    ASSERT_EQ("check", d.GetValue<std::string>());

    Any e = Any(std::string("dorou"));
    e = e;

    ASSERT_EQ("dorou", e.GetValue<std::string>());

    a.Swap(e);
    ASSERT_EQ(5, e.GetValue<int>());
}

void TestAll() {
    StartTesting();
    RUN_TEST(TestSimple);
    RUN_TEST(EmptyTest);
    RUN_TEST(CopyTest);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

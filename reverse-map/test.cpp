#include <testing.h>
#include <reverse_map.h>
#include <string>
#include <map>

using Map = std::map<std::string, int>;
using ReversedMap = std::map<int, std::string>;

namespace tests {

void Tests() {
    {
        Map test{{"aba", 3}, {"caba", 1}, {"test", 2}};
        ReversedMap expected{{1, "caba"}, {2, "test"}, {3, "aba"}};
        ASSERT_EQ(expected, ReverseMap(test));
    }
    {
        Map test{{"", 0}, {"1", 1}};
        ReversedMap expected{{0, ""}, {1, "1"}};
        ASSERT_EQ(expected, ReverseMap(test));
    }
}

void Empty() {
    Map test;
    ReversedMap expected;
    ASSERT_EQ(expected, ReverseMap(test));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Tests);
    RUN_TEST(Empty);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
